package com.vsotelo.spring.mail.util.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EnumServicesId {

    SERVICE_MAIL("MAIL_SERVICE_ID");

    private final String serviceId;
}
