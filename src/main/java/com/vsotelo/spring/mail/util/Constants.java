package com.vsotelo.spring.mail.util;

public class Constants {

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////      M A I L    C O N S T A N T S        ////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public static final String LOGO_SPRING = "https://miro.medium.com/max/1400/1*cUUJs9-zAcbWVrO6kiKGpw.png";
    public static final String UTF_8 = "UTF-8";
    public static final boolean MULTIPART_TRUE = true;
    public static final String MAIL_FROM = "andres.sotelo.meza@gmail.com";
    public static final String SEMICOLON = ";";
    public static final String SUCCESSFUL_OPERATION = "Successful operation";
}
