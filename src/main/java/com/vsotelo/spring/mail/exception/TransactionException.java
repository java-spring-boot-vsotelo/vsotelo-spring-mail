package com.vsotelo.spring.mail.exception;

import com.vsotelo.spring.mail.pojos.ObjectResponse;

public class TransactionException extends Exception {

    ObjectResponse<?> response;


    public TransactionException() {
    }

    public TransactionException(String string) {
        super(string);
        response = new ObjectResponse<>();
        response.setMessage(string);
        response.setException(new TransactionException());
    }

    public TransactionException(String string, Exception e) {
        super(string);
        response = new ObjectResponse<>();
        response.setMessage(string);
        response.setException(e);
    }

    public TransactionException(ObjectResponse<?> response) {
        super(response.getException());
        this.response = response;
    }

    public ObjectResponse<?> getResponse() {
        return response;
    }

}
