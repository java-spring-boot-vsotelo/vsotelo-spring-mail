package com.vsotelo.spring.mail.controller;

import com.vsotelo.spring.mail.exception.TransactionException;
import com.vsotelo.spring.mail.pojos.MailRequest;
import com.vsotelo.spring.mail.pojos.ObjectResponse;
import com.vsotelo.spring.mail.service.MailService;
import com.vsotelo.spring.mail.util.Constants;
import com.vsotelo.spring.mail.util.MessagesUtil;
import com.vsotelo.spring.mail.util.enums.EnumServicesId;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping("mail")
@RestController
@Tag(name = "mail-controller", description = "Mail Controller")
public class MailController {

    public static final Logger LOG = LogManager.getLogger(MailController.class);

    private final MailService mailService;

    @Autowired
    public MailController(MailService mailService) {
        this.mailService = mailService;
    }


    @Operation(summary = "Send mail", description = "Send mail", tags = "Mail")
    @ApiResponses(value = {@ApiResponse(description = "Successful operation", responseCode = "200")})
    @PostMapping(value = "/send-json", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectResponse<String>> sendJson(@RequestBody MailRequest mailRequest) throws Exception {
        long starTime = System.currentTimeMillis();
        ObjectResponse<String> response = new ObjectResponse<>();
        HttpStatus httpStatus;
        try {
            mailService.sendMail(mailRequest);
            response.setMessage(MessagesUtil.MSG_SUCCESS_SEND);
            response.setResult(Constants.SUCCESSFUL_OPERATION);
            response.setException(StringUtils.EMPTY);
            httpStatus = HttpStatus.OK;
        } catch (TransactionException e) {
            LOG.error(e.getMessage());
            httpStatus = HttpStatus.BAD_REQUEST;
            response.setMessage(e.getResponse().getMessage());
            response.setException(e.getResponse().getException());
        } catch (Exception e) {
            LOG.error(e.getMessage());
            response.setMessage(MessagesUtil.MSG_SYSTEM_ERROR);
            response.setException(e);
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        response.setStatusCode(httpStatus.value());
        response.setMethod("send");
        response.setServiceUrl("/mail/send");
        response.setServiceId(EnumServicesId.SERVICE_MAIL.getServiceId());

        long endTime = System.currentTimeMillis();
        LOG.info("[utility] Duration of execution (ms): {}", (endTime - starTime));
        return new ResponseEntity<>(response, httpStatus);
    }

    @Operation(summary = "Send mail", description = "Send mail", tags = "Mail")
    @ApiResponses(value = {@ApiResponse(description = "Successful operation", responseCode = "200")})
    @PostMapping(value = "/send-multipart-form-data", consumes = {"multipart/form-data"})
    public ResponseEntity<ObjectResponse<String>> sendMulti(@RequestParam String to, @RequestParam String cc, @RequestParam String subject, @RequestParam String body,
                                                            @RequestParam MultipartFile file, @RequestParam Boolean isHtml, @RequestParam Boolean noUseTemplate) {
        long starTime = System.currentTimeMillis();
        ObjectResponse<String> response = new ObjectResponse<>();
        HttpStatus httpStatus;
        try {
            mailService.sendMail(to, cc, subject, body, isHtml, noUseTemplate, file);
            response.setMessage(MessagesUtil.MSG_SUCCESS_SEND);
            response.setResult(Constants.SUCCESSFUL_OPERATION);
            httpStatus = HttpStatus.OK;
        } catch (TransactionException e) {
            LOG.error(e.getMessage());
            httpStatus = HttpStatus.BAD_REQUEST;
            response.setMessage(e.getResponse().getMessage());
            response.setException(e.getResponse().getException());
        } catch (Exception e) {
            LOG.error(e.getMessage());
            response.setMessage(MessagesUtil.MSG_SYSTEM_ERROR);
            response.setException(e);
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        response.setStatusCode(httpStatus.value());
        response.setMethod("send");
        response.setServiceUrl("/mail/send");
        response.setServiceId(EnumServicesId.SERVICE_MAIL.getServiceId());

        long endTime = System.currentTimeMillis();
        LOG.info("[utility] Duration of execution (ms): {}", (endTime - starTime));
        return new ResponseEntity<>(response, httpStatus);
    }
}
