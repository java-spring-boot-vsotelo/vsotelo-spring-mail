package com.vsotelo.spring.mail.service;

import com.vsotelo.spring.mail.pojos.MailRequest;
import org.springframework.web.multipart.MultipartFile;

public interface MailService {

    void sendMail(MailRequest mailRequest) throws Exception;

    void sendMail(String to, String cc, String subject, String body, Boolean isHtml, Boolean noUseTemplate, MultipartFile file) throws Exception;

}
