package com.vsotelo.spring.mail.service.impl;

import com.vsotelo.spring.mail.exception.TransactionException;
import com.vsotelo.spring.mail.pojos.MailRequest;
import com.vsotelo.spring.mail.service.MailService;
import com.vsotelo.spring.mail.util.Constants;
import com.vsotelo.spring.mail.util.MessagesUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.internet.MimeMessage;
import javax.mail.MessagingException;
import javax.mail.internet.MimeUtility;
import java.util.HashMap;
import java.util.Map;

import static j2html.TagCreator.body;
import static j2html.TagCreator.div;
import static j2html.TagCreator.html;
import static j2html.TagCreator.img;
import static j2html.TagCreator.link;
import static j2html.TagCreator.meta;
import static j2html.TagCreator.tag;

@Service
public class MailServiceImpl implements MailService {

    private static final Logger logger = LogManager.getFormatterLogger(MailServiceImpl.class);

    public final JavaMailSender emailSender;

    @Autowired
    public MailServiceImpl(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Override
    public void sendMail(MailRequest params) throws Exception {
        try {
            System.setProperty("mail.mime.split.long.parameters", "false");
            String logo = Constants.LOGO_SPRING;
            String mailBody = params.getBody();

            if (!params.isNoUseTemplate()) {
                params.setHtml(true);
                mailBody = html(
                        body(
                                meta().withCharset(Constants.UTF_8),
                                link().withHref("https://fonts.googleapis.com/css?family=Roboto").withRel("stylesheet"),
                                tag("center").with(
                                        div().withStyle("width:400px;border:1px solid #ddd;margin-top:30px;margin-bottom:30px;padding:15px;box-shadow:5px 5px 15px;").with(
                                                div(img().withStyle("display: inline-block;width:200px;height:auto;").withSrc(logo)
                                                ).withStyle("padding:3px;border-bottom: 2px solid #ddd;margin-bottom: 10px;"),
                                                div("{BODY}"),
                                                div("This email is automatic, if it was received by mistake,"
                                                        + " you can ignore this email.").withStyle("font-size:12px;color:#a09898;")
                                        )
                                )
                        ).withStyle("font-family: Roboto, sans-serif;")
                ).renderFormatted().replace("{BODY}", params.getBody());
            }
            MimeMessage message = emailSender.createMimeMessage();

            MimeMessageHelper helper = new MimeMessageHelper(message, Constants.MULTIPART_TRUE, Constants.UTF_8);
            helper.setFrom(Constants.MAIL_FROM);

            // receive email list in semicolons
            helper.setTo(params.getTo().split(Constants.SEMICOLON));
            if (params.getCc() != null) {
                helper.setCc(params.getCc());
            }
            if (params.getSubject() != null) {
                helper.setSubject(params.getSubject());
            }

            if (params.getAttached() != null) {
                if (!params.getAttached().isEmpty()) {
                    for (Map.Entry<String, byte[]> pdf : params.getAttached().entrySet()) {
                        helper.addAttachment(MimeUtility.encodeText(pdf.getKey()), new ByteArrayResource(pdf.getValue()));
                    }
                }
            }
            helper.setText(mailBody, params.isHtml());
            emailSender.send(message);
        } catch (MessagingException e) {
            logger.error(e.getMessage());
            throw new TransactionException(MessagesUtil.MSG_SYSTEM_ERROR);
        }
    }

    @Override
    public void sendMail(String to, String cc, String subject, String body, Boolean isHtml, Boolean noUseTemplate, MultipartFile file) throws Exception {

        if (StringUtils.isEmpty(to)) throw new TransactionException(MessagesUtil.MSG_TO_IS_BLANK);
        if (StringUtils.isEmpty(subject)) throw new TransactionException(MessagesUtil.MSG_TO_IS_SUBJECT);
        if (StringUtils.isEmpty(body)) throw new TransactionException(MessagesUtil.MSG_TO_IS_BODY);

        try {

            System.setProperty("mail.mime.split.long.parameters", "false");
            String logo = Constants.LOGO_SPRING;
            String mailBody = body;

            if (!noUseTemplate) {
                isHtml = true;
                mailBody = html(
                        body(
                                meta().withCharset(Constants.UTF_8),
                                link().withHref("https://fonts.googleapis.com/css?family=Roboto").withRel("stylesheet"),
                                tag("center").with(
                                        div().withStyle("width:400px;border:1px solid #ddd;margin-top:30px;margin-bottom:30px;padding:15px;box-shadow:5px 5px 15px;").with(
                                                div(img().withStyle("display: inline-block;width:400px;height:auto;").withSrc(logo)
                                                ).withStyle("padding:3px;border-bottom: 2px solid #ddd;margin-bottom: 10px;"),
                                                div("{BODY}"),
                                                div("This email is automatic, if it was received by mistake,"
                                                        + " you can ignore this email.").withStyle("font-size:12px;color:#a09898;")
                                        )
                                )
                        ).withStyle("font-family: Roboto, sans-serif;")
                ).renderFormatted().replace("{BODY}", body);
            }

            MimeMessage message = emailSender.createMimeMessage();

            MimeMessageHelper helper = new MimeMessageHelper(message, Constants.MULTIPART_TRUE, Constants.UTF_8);
            helper.setFrom(Constants.MAIL_FROM);

            // receive email list in semicolons
            helper.setTo(to.split(Constants.SEMICOLON));
            if (cc != null) {
                helper.setCc(cc.split(Constants.SEMICOLON));
            }

            helper.setSubject(subject);

            if (file != null) {
                HashMap<String, byte[]> mapFile = new HashMap<>();
                mapFile.put(file.getOriginalFilename(), file.getBytes());
                for (Map.Entry<String, byte[]> fileAttachment : mapFile.entrySet()) {
                    helper.addAttachment(MimeUtility.encodeText(fileAttachment.getKey()), new ByteArrayResource(fileAttachment.getValue()));
                }
            }
            helper.setText(mailBody, isHtml);
            emailSender.send(message);
        } catch (MessagingException e) {
            logger.error(e.getMessage());
            throw new TransactionException(MessagesUtil.MSG_SYSTEM_ERROR);
        }
    }
}
