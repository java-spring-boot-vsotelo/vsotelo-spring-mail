package com.vsotelo.spring.mail;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@SpringBootApplication
public class SpringApplication {

    public static void main(String[] args) { org.springframework.boot.SpringApplication.run(SpringApplication.class, args); }

    @Bean
    public OpenAPI customOpenAPI(@Value("${springdoc.version}") String appVersion) {
        return new OpenAPI()
                .info(new Info().title("VSOTELO [SPRING-MAIL] ").version(appVersion).description(
                        "This project is a demonstration of what can be done with spring mail, it can also be attached to be sent by mail.")
                        .termsOfService("http://swagger.io/terms/")
                        .license(new License().name("Apache 2.0").url("http://springdoc.org")));
    }
}
