package com.vsotelo.spring.mail.pojos;

import lombok.Data;

@Data
public class ObjectResponse<T> {

    private Integer statusCode;
    private String message;
    private String serviceId;
    private String serviceUrl;
    private String method;
    private String exception;
    private T result;


    public void setException(String ex) {
        this.exception = ex;
    }

    public void setException(Exception ex) {
        if (ex.getCause() != null) {
            this.exception = ex.getCause().toString();
        } else if (ex.getMessage() != null) {
            this.exception = ex.getMessage();
        } else {
            this.exception = ex.getClass().getName();
        }
    }

}
