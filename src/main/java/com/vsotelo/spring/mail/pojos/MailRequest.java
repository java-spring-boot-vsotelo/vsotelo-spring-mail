package com.vsotelo.spring.mail.pojos;

import lombok.Builder;
import lombok.Data;
import java.util.HashMap;

@Data
@Builder
public class MailRequest {

    private String to;
    private String cc;
    private String subject;
    private String body;
    private HashMap<String, byte[]> attached;
    private boolean html;
    private boolean noUseTemplate;
}
