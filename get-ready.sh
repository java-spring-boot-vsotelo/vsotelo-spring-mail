#!/bin/bash
echo -e "\n . get-ready.sh iDevExpert by vsotelo"

echo -e "  __   _______   _______ ____    ____  _______ ___   ___ .______    _______ .______      ._________. "
echo -e " |  | |       \ |   ____|\   \  /   / |   ____|\  \ /  / |   _  \  |   ____||   _  \     |        |  "
echo -e " |  | |  .--.  ||  |__    \   \/   /  |  |__    \  V  /  |  |_)  | |  |__   |  |_)  |    ---|  |---  "
echo -e " |  | |  |  |  ||   __|    \      /   |   __|    >   <   |   ___/  |   __|  |      /        |  |     "
echo -e " |  | |  '--'  ||  |____    \    /    |  |____  /  .  \  |  |      |  |____ |  |\  \---     |  |     "
echo -e " |__| |_______/ |_______|    \__/     |_______|/__/ \__\ | _|      |_______|| _| ._____|    |__|     "
echo -e " ____________________________________________________________________________________________________"

# Functions
function help {
  echo -e " -----------------------------------------------------------------------------------\n"
  echo -e " -------------------------------------  OPTIONS ------------------------------------\n"
  echo -e " ---- This file has been create by vsotelo"
  echo -e "  List of commands: \n"
  echo -e "    -run: generate image and run docker container. \n"
  echo -e " -----------------------------------------------------------------------------------\n"
  exit
}


function run {
  mvn package
	mvn clean install && docker build . -t springmail
	docker-compose up
}

# COLLECTING THE CONFIGURATION PARAMETERS
if [ "$#" -ne 0 ]; then
	# shellcheck disable=SC2034
	# shellcheck disable=SC2116
	COMMAND=$(echo "$1")
	echo -e " .. command: COMMAND"

	#RUNNING DOCKER OPERATION
	# shellcheck disable=SC2194
	case COMMAND in
		"run")
			run
		;;
		*)
			help
		;;
	esac

fi

echo -e "\n"

