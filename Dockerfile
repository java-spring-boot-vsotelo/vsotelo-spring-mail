# Base Image
FROM openjdk:8-jdk-alpine

# Author
LABEL MAINTAINER = "andres.sotelo.meza@gmail.com"

# Set Time Zone
RUN apk add --no-cache tzdata
ENV TZ=America/Lima
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Puertos expuestos
EXPOSE 8080
EXPOSE 8000

# Temporary volume where Spring Boot creates working directories for Tomcat by default.
VOLUME /tmp

# Arguments
ARG DEPLOY_PROFILE=dev
ARG JAR_FILE=target/*.jar
ADD ${JAR_FILE} app.jar

# Run --> /**need to configure profiles in maven**/
ENTRYPOINT java $JAVA_OPTS \
			-agentlib:jdwp=transport=dt_socket,address=8000,server=y,suspend=n \
			-Djava.security.egd=file:/dev/./urandom \
			-Dspring.profiles.active=${DEPLOY_PROFILE} \
			${PROFILING} -jar /app.jar